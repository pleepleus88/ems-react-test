import React from 'react';

import Subheader from 'material-ui/Subheader';

import Booking from './Booking.component';

import FormattedDate from '../../shared/FormattedDate';

export default function BookingDate({ bookingDate }) {
    return (
        <div>
            <Subheader
                style={{
                    backgroundColor: '#eee',
                    color: '#999',
                    textTransform: 'uppercase'
                }}
            >
                <FormattedDate date={bookingDate.date} format="ddd MMM DD" />
            </Subheader>
            {bookingDate.bookings.map(booking => (
                <Booking booking={booking} key={booking.id} />
            ))}
        </div>
    );
}
