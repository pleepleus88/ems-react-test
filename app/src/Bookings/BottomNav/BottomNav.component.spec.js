import { shallow } from 'enzyme';
import React from 'react';

import BottomNav from './BottomNav.component';

describe('BottomNav.component', () => {
    describe('no bookings', () => {
        it('doesnt render Now button', () => {
            const wrapper = shallow(<BottomNav bookings={[]} />);

            expect(wrapper.find('#now').length).toEqual(0);
        });
    });

    describe('bookings', () => {
        const bookings = [
            {
                id: 'an id',
                eventName: 'an event',
                roomName: 'a room',
                start: '2016-11-10',
                end: '2016-11-10'
            }
        ];
        it('shows Now button', () => {
            const wrapper = shallow(<BottomNav bookings={bookings} />);

            expect(wrapper.find('#now').length).toEqual(1);
        });
    });
});
