import groupBookingsByDate from './group-bookings-by-date';

describe('groupBookingsByDate', () => {
    it('extracts date from single booking', () => {
        const input = [dayOneBookingA];

        const result = groupBookingsByDate(input);

        expect(result.length).toEqual(1);
        expect(result[0].date).toEqual('2016-11-10');
    });

    it('extracts all dates from bookings', () => {
        const input = [dayOneBookingA, dayTwoBooking];

        const result = groupBookingsByDate(input);

        expect(result.length).toEqual(2);
        expect(result[0].date).toEqual('2016-11-10');
        expect(result[1].date).toEqual('2016-11-14');
    });

    it('groups bookings from same date', () => {
        const input = [dayOneBookingA, dayOneBookingB, dayTwoBooking];

        const result = groupBookingsByDate(input);

        expect(result.length).toEqual(2);
        expect(result[0].bookings.length).toEqual(2);
        expect(result[1].bookings.length).toEqual(1);
    });

    it('maps bookings to correct shape', () => {
        const input = [dayOneBookingA];

        const result = groupBookingsByDate(input);

        expect(result.length).toEqual(1);
        expect(result[0].bookings.length).toEqual(1);
        const booking = result[0].bookings[0];
        expect(booking.id).toEqual(dayOneBookingA.id);
        expect(booking.eventName).toEqual(dayOneBookingA.eventName);
        expect(booking.roomName).toEqual(dayOneBookingA.roomName);
        expect(booking.start).toEqual(dayOneBookingA.start);
        expect(booking.end).toEqual(dayOneBookingA.end);
    });
});

const dayOneBookingA = {
    id: 0,
    eventName: 'Test Booking 01',
    roomName: 'Demo Room 01',
    start: '2016-11-10T13:00:00.000Z',
    end: '2016-11-10T14:00:00.000Z'
};
const dayOneBookingB = {
    id: 1,
    eventName: 'Test Booking 02',
    roomName: 'Demo Room 02',
    start: '2016-11-10T13:00:00.000Z',
    end: '2016-11-10T14:00:00.000Z'
};
const dayTwoBooking = {
    id: 2,
    eventName: 'Test Booking 03',
    roomName: 'Demo Room 03',
    start: '2016-11-14T13:00:00.000Z',
    end: '2016-11-14T14:00:00.000Z'
};
