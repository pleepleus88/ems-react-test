import React from 'react';

import Nav from './Nav.component.js';

class CalendarContainer extends React.Component {
    state = {
        showCalendar: false
    };

    render() {
        return (
            <Nav
                {...this.state}
                {...this.props}
                onCalendarToggled={this.handleCalendarToggled}
            />
        );
    }

    handleCalendarToggled = () => {
        this.setState(prevState => ({
            showCalendar: !prevState.showCalendar
        }));
    };
}

export default CalendarContainer;
