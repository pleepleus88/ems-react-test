import React from 'react';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';

export default function BottomNav(props) {
    return (
        <footer>
            <Toolbar style={{ justifyContent: 'center' }}>
                <ToolbarGroup>{renderNowButton(props)}</ToolbarGroup>
            </Toolbar>
        </footer>
    );
}

function renderNowButton({ bookings }) {
    if (!bookings || bookings.length === 0) {
        return null;
    }

    return <FlatButton id="now">Now</FlatButton>;
}
