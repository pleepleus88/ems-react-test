import { shallow } from 'enzyme';
import React from 'react';

import Nav from './Nav.component';

describe('Nav.component', () => {
    describe('no bookings', () => {
        it('doesnt show month picker', () => {
            const wrapper = shallow(<Nav bookings={[]} />);

            expect(wrapper.find('#month-picker').length).toEqual(0);
        });

        it('doesnt show search icon', () => {
            const wrapper = shallow(<Nav bookings={[]} />);

            expect(wrapper.find('.fa-search').length).toEqual(0);
        });
    });

    describe('bookings', () => {
        const bookings = [
            {
                id: 'an id',
                eventName: 'an event',
                roomName: 'a room',
                start: '2016-11-10',
                end: '2016-11-10'
            }
        ];
        it('shows month picker', () => {
            const wrapper = shallow(<Nav bookings={bookings} />);

            expect(wrapper.find('#month-picker').length).toEqual(1);
        });

        it('shows search icon', () => {
            const wrapper = shallow(<Nav bookings={bookings} />);

            expect(wrapper.find('.fa-search').length).toEqual(1);
        });
    });
});
