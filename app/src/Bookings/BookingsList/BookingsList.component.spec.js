import { shallow } from 'enzyme';
import React from 'react';

import BookingsList from './BookingsList.component';

describe('BookingsList.component', () => {
    describe('no bookings', () => {
        it('shows no bookings message', () => {
            const wrapper = shallow(<BookingsList bookingDates={[]} />);

            expect(wrapper.find('#no-bookings').length).toEqual(1);
        });

        it('doesnt show bookings list', () => {
            const wrapper = shallow(<BookingsList bookingDates={[]} />);

            expect(wrapper.find('#bookings-list').length).toEqual(0);
        });
    });

    describe('booking dates', () => {
        const bookingDates = [
            {
                date: '2016-11-10',
                bookings: [
                    {
                        id: 0,
                        eventName: 'Test Booking 01',
                        roomName: 'Demo Room 01',
                        start: '2016-11-10T13:00:00.000Z',
                        end: '2016-11-10T14:00:00.000Z'
                    }
                ]
            }
        ];

        it('doesnt show no bookings message', () => {
            const wrapper = shallow(
                <BookingsList bookingDates={bookingDates} />
            );

            expect(wrapper.find('#no-bookings').length).toEqual(0);
        });

        it('shows bookings list', () => {
            const wrapper = shallow(
                <BookingsList bookingDates={bookingDates} />
            );

            expect(wrapper.find('#bookings-list').length).toEqual(1);
        });
    });
});
