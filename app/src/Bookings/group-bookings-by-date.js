import moment from 'moment';
import groupBy from 'lodash.groupby';

//expected input:
// [
//     {
//         "id": 0,
//         "eventName": "Test Booking 01",
//         "roomName": "Demo Room 01",
//         "start": "2016-11-10T13:00:00.000Z",
//         "end": "2016-11-10T14:00:00.000Z"
//     },
//     //....
// ]

//expected output:
// [
//     {
//         date: "2016-11-10",
//         bookings: [
//             {
//                 "id": 0,
//                 "eventName": "Test Booking 01",
//                 "roomName": "Demo Room 01",
//                 "start": "2016-11-10T13:00:00.000Z",
//                 "end": "2016-11-10T14:00:00.000Z"
//             },
//             //...
//         ]
//     },
//     //...
// ]

export default function groupBookingsByDate(bookings) {
    const bookingsWithDates = addDateToBookings(bookings);

    const groupedByDate = groupBy(bookingsWithDates, 'date');

    return mapGroupsToResults(groupedByDate);
}

function addDateToBookings(bookings) {
    return bookings.map(booking => ({
        date: parseDate(booking.start),
        booking
    }));
}

function parseDate(booking) {
    const bookingMoment = moment(booking, moment.ISO_8601);

    return bookingMoment.format('YYYY-MM-DD');
}

function mapGroupsToResults(groups) {
    return Object.keys(groups).map(key => ({
        date: key,
        bookings: groups[key].map(bookingWithDate => bookingWithDate.booking)
    }));
}
