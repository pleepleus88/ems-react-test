import React from 'react';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import NavigationExpandLessIcon from 'material-ui/svg-icons/navigation/expand-less';

import Calendar from '../Calendar';

export default function Nav(props) {
    return (
        <header>
            <Toolbar>
                <ToolbarGroup>{renderMonthPicker(props)}</ToolbarGroup>
                <ToolbarGroup>
                    {renderSearchButton(props)}
                    <i className="fa fa-plus" />
                </ToolbarGroup>
            </Toolbar>
            {renderCalendar(props)}
        </header>
    );
}

function renderCalendar({ showCalendar }) {
    if (!showCalendar) {
        return null;
    }

    return <Calendar />;
}

function renderMonthPicker({ bookings, showCalendar, onCalendarToggled }) {
    if (!bookings || bookings.length === 0) {
        return null;
    }

    return (
        <div id="month-picker">
            <a onClick={onCalendarToggled}>
                <ToolbarTitle text="March 2016" />
            </a>
            <IconButton touch={true} onClick={onCalendarToggled}>
                {renderIcon(showCalendar)}
            </IconButton>
        </div>
    );
}

function renderSearchButton({ bookings }) {
    if (!bookings || bookings.length === 0) {
        return null;
    }

    return <i className="fa fa-search" />;
}

function renderIcon(showCalendar) {
    if (!showCalendar) {
        return <NavigationExpandMoreIcon />;
    }

    return <NavigationExpandLessIcon />;
}
