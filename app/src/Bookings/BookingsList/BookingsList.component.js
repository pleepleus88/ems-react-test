import React from 'react';
import { List } from 'material-ui/List';

import BookingDate from './BookingDate.component';

export default function BookingsList({ bookingDates }) {
    if (!bookingDates || bookingDates.length === 0) {
        return renderNoBookings();
    }

    return (
        <List
            id="bookings-list"
            style={{
                padding: '0 0',
                borderTop: '1px solid #777',
                borderBottom: '1px solid #777'
            }}
        >
            {bookingDates.map(bookingDate => (
                <BookingDate bookingDate={bookingDate} key={bookingDate.date} />
            ))}
        </List>
    );
}

function renderNoBookings() {
    return <div id="no-bookings">There are currently no bookings.</div>;
}
