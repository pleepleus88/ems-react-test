import moment from 'moment';
require('moment-duration-format');

export default function GetBookingDuration(booking) {
    const parsedStart = moment(booking.start);
    const parsedEnd = moment(booking.end);
    const duration = parsedEnd.diff(parsedStart, 'minutes');

    //moment-duration-format doesn't clean up trailing units
    // that are equal to zero. (i.e. 1h instead of 1h 0m)
    //There might be a setting that does it, but I couldn't find one, and ran out of time.
    // I'd reconsider using moment-duration-format based on how much work we have to do here anyway.
    let format = 'm[m]';
    if (duration >= 60) {
        if (duration % 60 === 0) {
            format = 'h[h]';
        } else {
            format = 'h[h] m[m]';
        }
    }
    return moment.duration(duration, 'minutes').format(format);
}
