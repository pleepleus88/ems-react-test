import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import '../../node_modules/font-awesome/css/font-awesome.min.css';

import Bookings from '../Bookings';

class App extends Component {
    render() {
        return (
            <MuiThemeProvider>
                <Bookings />
            </MuiThemeProvider>
        );
    }
}

export default App;
