import React from 'react';

import { ListItem } from 'material-ui/List';

import FormattedDate from '../../shared/FormattedDate';
import BookingDuration from './BookingDuration.component';

export default function Booking({ booking }) {
    return (
        <ListItem style={{ borderBottom: '1px solid #999' }}>
            <div
                style={{
                    display: 'table',
                    width: '100%'
                }}
            >
                <div style={{ display: 'table-cell', width: '30%' }}>
                    <div>
                        <FormattedDate date={booking.start} format="LT" />
                    </div>
                    <div>
                        <FormattedDate date={booking.end} format="LT" />
                    </div>
                    <div>
                        <BookingDuration booking={booking} />
                    </div>
                </div>
                <div style={{ display: 'table-cell', width: '70%' }}>
                    <div>{booking.eventName}</div>
                    <div>{booking.roomName}</div>
                </div>
            </div>
        </ListItem>
    );
}
