# Summary

This doc contains the steps I took/will take to build the app.

I restricted myself to 8 hours of work. There are many details to the app that could have resulted in me spending much more time, and I didn't feel it was appropriate to go beyond 8 hours.

The app is in a folder named ./app, off the root.

## Bootstrap the app

I chose Create-react-app. For a tech exercise, this seemed to be a no-brainer. It gets me an app spun up quickly.

If we wanted to make this thing live longer, I would consider ejecting Create-react-app at an early stage, so that we had control over all the config.

## Material UI

I need to research quickly how to do this. The apps I've built have typically used Bootstrap, or completely custom CSS. I suspect that I can npm install a dependency of some sort to make this happen pretty easily.

Update: It is at this point that I noticed in the original README that http://www.material-ui.com is called out specifically as the library to pull in. I was thinking I had to choose from multiple implementations of Google Material Design. Hooray for easy decisions!

### Roboto

The material-ui docs suggest including the Roboto font. I chose to pull these in from the Google Fonts CDN, instead of downloading & putting in my code-base. I made this decision for speed of development. A long-term app might warrant pulling the actual font files into our project.

## Font-awesome

At this point, I noticed font-awesome was requested for icons. I pulled this in via npm, & imported it.

## Build the UI

My favorite page in the React docs is the page that describes how to think in terms of React. Per those docs, and as I've experienced to be the best approach, I will initially build the app out with hardcoded state.

In the process of building the UI, I've noticed a few extra pieces of complexity.

* The month is displayed in the header. My guess is that this should vary based on where the user is scrolled in the list. I would probably use Redux to communicate this state from the Bookings component to the Nav component.

* Bookings are grouped by date.

* Un-booked dates are grouped together in the UI. I will save this for the end, and probably won't get to it.

* There is a calendar icon next to one of the bookings in the mockups. I don't know what this is, or what it means. I don't see anything about that date that is different than other dates. It might show up when I hover over that item???? I will probably not address this.

## Styling

In a long-term project, I'd consider at this point how we were going to handle styling. I'm partial to using SCSS, but that doesn't appear to play well with material-ui. Material-ui works well with inline styles, which I am not a fan of. Now would be a good time to decide the tech we'd use moving forward. This would be a team conversation.

Material-ui has definitely been my biggest pain-point so far. I will probably skip some fine-tuning of the styles, to get more functionality done.

## Add state

Looking at the app, I see both app-level state and component-level state.

### Component-level state

The entire datepicker would be component-level state if I built it myself. Since I pulled one in, the component state was managed for me.

There is component state in the appearance/disappearance of the datepicker. I use React setState to manage this state, as it doesn't affect other areas of the app, and doesn't have far in the component tree to travel.

### App-level state

There are a handful of components that are interested in the current selected date. Had I had the time, I would have used Redux to...

1. Pass the currently selected date into the datepicker
1. Fire off an action when a new date was selected
    1. Update the new selected date in the Redux store
1. Scroll the bookings list to the selected date when it changes
1. Fire off an action when the "now" button was clicked at the bottom
    1. Update the selected date to "today" in the Redux store

As I held myself to 8 hours of work, I did not have time to do this.

## Calendar

I suspect this exercise helps determine if the developer would try to roll their own calendar, or pull in a 3rd party library. I plan on pulling in a 3rd party calendar because dates & calendars are time-consuming. If this were a long-term shippable product, I might reconsider that. In 18 years of development I've yet to see a 3rd party datepicker that works correctly for all our requirements. To be fair - I've also never *built* a datepicker that works correctly for all our requirements. They're just difficult.

Update: I've opted to pull in [react-datepicker](https://hacker0x01.github.io/react-datepicker/) for the calendar functionality. It isn't styled according to Material Design, but I think we could invest more time in making it align visually. I've begun overriding some of the included styles in Calendar.component.css.

Material-ui [includes a datepicker](http://www.material-ui.com/#/components/date-picker) but according to the docs, it displays an input field, and clicking that shows the datepicker either in a modal, or overlaying the current form. This doesn't match the specs in the README, so I opted for a datepicker I had more control over displaying.

On a long-term project, I would want us to look at one of the following options:

1. Finding a 3rd party datepicker that both looked like Material Design, and could be displayed inline the way the specs show
1. Finding a 3rd party datepicker that was more focused on functionality than display, i.e. using "render" props, [as described here](https://blog.kentcdodds.com/how-to-give-rendering-control-to-users-with-prop-getters-549eaef76acf)
1. Building a datepicker that was more focused on functionality than display, i.e. using "render" props

## Group bookings by date

I pulled in lodash.groupBy to do a group by, for speed of development's sake. We could have talked about whether we wanted to avoid use of lodash, etc, in favor of using array.reduce.

## Immutable

Aside from material-ui, Immutable is the biggest unknown for me. I don't have any experience with it. I was hoping to pull it in after I got features built, but there were a lot features to build. Thus, I did not incorporate Immutable. Obviously in a long-term app this would have been different, but I wanted my results to be more about what I could build in 8 hours than how much of Immutable I could learn in 8 hours.

## Collapse un-booked dates into ranges, i.e. "Thur Apr 1 - Mon Apr - 5: No bookings" in UI.

Un-booked dates are grouped together in the UI. This seemed to me a secondary feature, so I left it for the end. I did not get to it.

## Fine-tune the UI

If I had more time, I would fine-tune the UI. The datepicker, for instance, doesn't look like material design.

The difference between this exercise and a real life project is that I plan on stopping at 8 hours. If this were a real life project, the UI would be more important.

### Get colors from variables

I hard-coded colors a couple of places. With more time, I'd get them from variables, as described here -
http://www.material-ui.com/#/customization/colors

