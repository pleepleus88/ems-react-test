import getBookingDuration from './get-booking-duration';

describe('getBookingDuration', () => {
    it('shows 15m for 15 minutes', () => {
        const start = '2017-01-01T00:00:00.000Z';
        const end = '2017-01-01T00:15:00.000Z';

        const result = getBookingDuration({ start, end });

        expect(result).toEqual('15m');
    });

    it('shows 1h for 60 minutes', () => {
        const start = '2017-01-01T00:00:00.000Z';
        const end = '2017-01-01T01:00:00.000Z';

        const result = getBookingDuration({ start, end });

        expect(result).toEqual('1h');
    });

    it('shows 1h 30m for 90 minutes', () => {
        const start = '2017-01-01T00:00:00.000Z';
        const end = '2017-01-01T01:30:00.000Z';

        const result = getBookingDuration({ start, end });

        expect(result).toEqual('1h 30m');
    });
});
