import React from 'react';

import moment from 'moment';

export default function FormattedDate({ date, format }) {
    const parsed = moment(date);

    return <span>{parsed.format(format)}</span>;
}
