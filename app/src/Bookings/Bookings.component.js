import React from 'react';

import Nav from './Nav';
import BookingsList from './BookingsList';
import BottomNav from './BottomNav';

import data from './data';
import groupBookingsByDate from './group-bookings-by-date';

export default function Bookings() {
    const bookingsByDate = groupBookingsByDate(data.bookings);

    return (
        <div>
            <Nav bookings={data.bookings} />
            <BookingsList bookingDates={bookingsByDate} />
            <BottomNav bookings={data.bookings} />
        </div>
    );
}
