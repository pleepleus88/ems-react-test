import React from 'react';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './Calendar.component.css';

export default function Calendar() {
    return (
        <div>
            <DatePicker
                inline
                /* selected={this.state.startDate} */
                /* onChange={this.handleChange} */
            />
        </div>
    );
}
