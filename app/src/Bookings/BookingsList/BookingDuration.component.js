import React from 'react';
import getBookingDuration from './get-booking-duration';

export default function BookingDuration({ booking }) {
    return <span>{getBookingDuration(booking)}</span>;
}
